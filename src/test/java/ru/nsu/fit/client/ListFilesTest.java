package ru.nsu.fit.client;

import ru.nsu.fit.client.domain.HomediaContentFile;
import ru.nsu.fit.client.domain.HomediaDevice;
import ru.nsu.fit.client.domain.HomediaFile;
import ru.nsu.fit.client.upnp.server.UpnpHomediaService;

import java.util.List;
import java.util.concurrent.TimeUnit;

public class ListFilesTest {

    public static void main(String[] args) {
        UpnpHomediaService service = new UpnpHomediaService();

        try {
            TimeUnit.SECONDS.sleep(5);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        List<HomediaDevice> devices = service.locateDevices();

        System.out.println("Found " + devices.size() + " devices");

        for (HomediaDevice device : devices) {
            System.out.println("-------------------------------------------");

            List<HomediaFile> deviceFiles = device.listFiles();

            for (HomediaFile file : deviceFiles) {
                System.out.println(file.getName());

                if (file.getName().equals("SOME_RANDOM_FILE.txt")) {
                    byte[] loadedFileBytes = ((HomediaContentFile) deviceFiles.get(deviceFiles.size() - 2)).loadBytes();

                    if (null != loadedFileBytes) {
                        System.out.println("File size in bytes: " + loadedFileBytes.length);
                    } else {
                        System.out.println("Failed to load file bytes.");
                    }
                }
            }
        }

    }
}
