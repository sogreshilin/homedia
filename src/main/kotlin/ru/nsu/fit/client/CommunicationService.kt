package ru.nsu.fit.client

import io.ktor.client.HttpClient
import io.ktor.client.engine.apache.Apache
import io.ktor.client.features.BadResponseStatusException
import io.ktor.client.features.json.JacksonSerializer
import io.ktor.client.features.json.JsonFeature
import io.ktor.client.request.get
import io.ktor.client.request.parameter
import io.ktor.client.request.post
import io.ktor.client.response.HttpResponse
import io.ktor.client.response.readText
import io.ktor.http.HttpStatusCode
import kotlinx.coroutines.runBlocking
import ru.nsu.fit.client.domain.UserGroup
import ru.nsu.fit.interaction.*

interface CommunicationService : UserService, GroupService

class CommunicationServiceImpl(val baseUrl: String) : CommunicationService {

	private var listeners: Set<GroupListChangedListener> = HashSet()

	override fun addGroupChangeListener(listener: GroupListChangedListener) {
		listeners += listener
	}

	val client = HttpClient(Apache) {
		install(JsonFeature) {
			serializer = JacksonSerializer()
		}
	}

	override fun getUserGroups(userId: Long): IntArray {
		return runBlocking {
			client.get<IntArray>("$baseUrl/user/$userId/groups")
		}
	}

	override fun getGroupInfo(groupId: Int): UserGroup? {
		return runBlocking {
			catching {
				client.get<UserGroup>("$baseUrl/group/$groupId")
			}
		}
	}


	override fun login(login: String, password: String): Long? {
		if (login == "1" && password == "1") {
			return Long.MAX_VALUE
		}
		return runBlocking {
			val post = client.post<HttpResponse>("$baseUrl/login") {
				parameter("login", login)
				parameter("pass", password)
			}
			post.readText().toLongOrNull()
		}
	}

	override fun register(user: UserDto): Long? {
		if (user.login == "1") return null
		return runBlocking {
			val post = client.post<HttpResponse>("$baseUrl/register") {
				parameter("name", user.name)
				parameter("login", user.login)
				parameter("pass", user.pass)
			}
			post.readText().toLongOrNull()
		}
	}

	override fun findUser(id: Long): UserDto? {
		if (id == Long.MAX_VALUE) {
			return UserDto("1", "1", "1")
		}
		return runBlocking {
			catching {
				client.get<UserDto>("$baseUrl/user/$id")
			}
		}
	}

	override fun hasUserWithLogin(login: String): Boolean {
		if (login == "1") return true
		return runBlocking {
			val response = client.get<HttpResponse>("$baseUrl/user/login/$login")
			response.status == HttpStatusCode.OK
		}
	}

	override fun createGroup(group: GroupDto): Int? {
		return runBlocking {
			client.post<HttpResponse>("$baseUrl/create_group") {
				parameter("pass", group.pass ?: "")
				parameter("name", group.name)
				parameter("description", group.description)
			}.readText().toIntOrNull()
		}
	}

	override fun existsWithName(name: String): Boolean {
		return runBlocking {
			val response = client.get<HttpResponse>("$baseUrl/groups/$name")
			response.status == HttpStatusCode.OK
		}
	}

	override fun joinGroup(userId: Long, groupId: Int, password: String?): Boolean {
		val joined =  runBlocking {
			client.post<HttpResponse>("$baseUrl/join_group") {
				parameter("groupId", groupId)
				parameter("userId", userId)
				parameter("password", password ?: "")
			}.status == HttpStatusCode.OK
		}
        if (joined) {
            listeners.forEach { it.onJoinGroup() }
        }
        
        return joined
	}

	override fun getGroupMemberIds(groupId: Int): LongArray {
		return runBlocking {
			client.get<LongArray>("$baseUrl/group/$groupId/users")
		}
	}

	override fun getAllGroups(): List<UserGroup> {
		return runBlocking {
			client.get<List<UserGroup>>("$baseUrl/groups")
		}
	}
}

suspend fun <T> catching(b: suspend ()->T): T? {
	return try {
		b()
	} catch (e: BadResponseStatusException) {
		null
	}
}