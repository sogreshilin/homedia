package ru.nsu.fit.server

class User(
    val login: String,
    val pass: String,
    val name: String,
    val id: Long?
)

class Group(
    val name: String,
    val description: String,
    val id: Int?,
    val password: String?
)