package ru.nsu.fit.server

import org.jetbrains.exposed.sql.Table

object Users : Table() {
    val id = long("id").autoIncrement().primaryKey()
    val name = varchar("name", length = 50)
    val login = varchar("login", length = 50).uniqueIndex()
    val pass = varchar("pass", length = 50)
}

object Groups : Table() {
    val id = integer("id").autoIncrement().primaryKey()
    val name = varchar("name", length = 50).uniqueIndex()
    val description = text("description")
    val password = text("password").nullable()
}

object UserToGroups : Table() {
    val userId = (long("user_id") references Users.id)
    val groupId = (integer("group_id") references Groups.id)

}