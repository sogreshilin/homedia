package ru.nsu.fit.server

import io.ktor.application.call
import io.ktor.application.install
import io.ktor.features.ContentNegotiation
import io.ktor.http.HttpStatusCode
import io.ktor.jackson.jackson
import io.ktor.locations.KtorExperimentalLocationsAPI
import io.ktor.locations.Locations
import io.ktor.locations.get
import io.ktor.locations.post
import io.ktor.response.respond
import io.ktor.response.respondText
import io.ktor.routing.get
import io.ktor.routing.routing
import io.ktor.server.engine.embeddedServer
import io.ktor.server.netty.Netty
import org.jetbrains.exposed.sql.Database
import org.jetbrains.exposed.sql.SchemaUtils
import org.jetbrains.exposed.sql.transactions.transaction

@UseExperimental(KtorExperimentalLocationsAPI::class)
fun main(args: Array<String>) {
    Database.connect("jdbc:h2:file:~/sample;", driver = "org.h2.Driver")

    transaction {
        SchemaUtils.create(Users)
        SchemaUtils.create(Groups)
        SchemaUtils.create(UserToGroups)
    }

    embeddedServer(Netty, 8080) {
        install(Locations.Feature)
        install(ContentNegotiation) {
            jackson {
            }
        }
        routing {
            trace {
                println(it.buildText())
            }
            post<RegisterInfo> {
                val userId = Queries.saveUser(User(it.login, it.pass, it.name, null))
                println("Register $userId")
                call.respondText(userId?.toString() ?: "User already exists")
            }
            post<LoginInfo> {
                val userId = Queries.findUserByLogin(it.login)
                if (userId == null) {
                    call.respond(HttpStatusCode.NotFound, "User not found")
                    return@post
                }
                val user = Queries.findUser(userId)
                if (it.pass != user?.pass) {
                    call.respond(HttpStatusCode.Unauthorized, "Password doesn't match")
                    return@post
                }
                call.respondText(userId.toString())
            }
            get<UserGroupsInfo> {
                call.respond(Queries.getGroupsForUser(it.id))
            }
            get<GroupInfo> {
                val groupInfo = Queries.findGroup(it.id)
                if (groupInfo != null) {
                    call.respond(groupInfo)
                } else {
                    call.respond(HttpStatusCode.NotFound)
                }
            }
            get<GroupByNameInfo> {
                val groupInfo = Queries.findGroupByName(it.name)
                if (groupInfo != null) {
                    call.respond(groupInfo)
                } else {
                    call.respond(HttpStatusCode.NotFound)
                }
            }
            post<JoinGroupInfo> {
                val group = Queries.findGroup(it.groupId)
                if (group == null) {
                    call.respond(HttpStatusCode.NotFound, "Group not exists")
                    return@post
                }
                if (it.password != group.password) {
                    call.respond(HttpStatusCode.Unauthorized, "Password doesn't match")
                    return@post
                }
                if (Queries.findUser(it.userId) == null) {
                    call.respond(HttpStatusCode.NotFound, "User not exists")
                }
                Queries.addUserToGroupUnchecked(it.userId, it.groupId)
                call.respond(true)
            }
            post<CreateGroupInfo> {
                val groupId = Queries.createGroup(Group(it.name, it.description, null, it.pass))
                call.respondText(groupId.toString())
            }
            get<UserInfo> {
                val user = Queries.findUser(it.id)
                if (user == null) {
                    call.respond(HttpStatusCode.NotFound, "User not found")
                } else {
                    call.respond(user)
                }
            }
            get<UserByLoginInfo> {
                val userId = Queries.findUserByLogin(it.login)
                if (userId == null) {
                    call.respond(HttpStatusCode.NotFound, "User not found")
                } else {
                    call.respondText(userId.toString())
                }
            }
            get<GroupMembersInfo> {
                call.respond(Queries.findGroupMembers(it.id))
            }
            get("/groups") {
                call.respond(Queries.findAllGroups())
            }
        }
    }.start(wait = true)
}