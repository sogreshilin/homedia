package ru.nsu.fit.server

import io.ktor.locations.Location

@Location("/register")
data class RegisterInfo(
    val name: String,
    val login: String,
    val pass: String
)

@Location("/user/{id}")
data class UserInfo(
		val id: Long
)

@Location("/user/login/{login}")
data class UserByLoginInfo(
		val login: String
)

@Location("/login")
data class LoginInfo(
    val login: String,
    val pass: String
)

@Location("/create_group")
data class CreateGroupInfo(
    val name: String,
    val description: String,
    val pass: String?
)

@Location("/groups/{name}")
data class GroupByNameInfo(
		val name: String
)

@Location("/join_group")
data class JoinGroupInfo(
    val groupId: Int,
    val userId: Long,
    val password: String?
)

@Location("/user/{id}/groups")
data class UserGroupsInfo(
    val id: Long
)

@Location("/group/{id}")
data class GroupInfo(
    val id: Int
)

@Location("/group/{id}/users")
data class GroupMembersInfo(
    val id: Int
)

