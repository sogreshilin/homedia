package ru.nsu.fit.server

import org.jetbrains.exposed.sql.insert
import org.jetbrains.exposed.sql.select
import org.jetbrains.exposed.sql.selectAll
import org.jetbrains.exposed.sql.transactions.transaction

object Queries {
    fun saveUser(user: User): Long? {
        return try {
            transaction {
                Users.insert {
                    it[name] = user.name
                    it[login] = user.login
                    it[pass] = user.pass
                } get Users.id ?: -1
            }
        } catch (e: Throwable) {
            null
        }
    }

    fun findUserByLogin(login: String): Long? {
        return transaction {
            Users.select { Users.login.eq(login) }
                .firstOrNull()
                ?.get(Users.id)
        }
    }

    fun findUser(id: Long): User? {
        return transaction {
            Users.select { Users.id.eq(id) }
                .firstOrNull()
                ?.let {
                    User(
                        it[Users.login],
                        it[Users.pass],
                        it[Users.name],
                        it[Users.id]
                    )
                }
        }
    }

    fun getGroupsForUser(userId: Long): List<Int> {
        return transaction {
            UserToGroups
                    .select { UserToGroups.userId.eq(userId) }
                    .map { it[UserToGroups.groupId] }
        }
    }

    fun findGroup(id: Int): Group? {
        return transaction {
            Groups.select { Groups.id.eq(id) }
                .firstOrNull()
                ?.let {
                    Group(
                        it[Groups.name],
                        it[Groups.description],
                        it[Groups.id],
                        it[Groups.password]
                    )
                }
        }
    }

    fun findGroupByName(name: String): Group? {
        return transaction {
            Groups.select { Groups.name.eq(name) }
                .firstOrNull()
                ?.let {
                    Group(
                        it[Groups.name],
                        it[Groups.description],
                        it[Groups.id],
                        it[Groups.password]
                    )
                }
        }
    }

    fun createGroup(group: Group) : Int? {
        return try {
            transaction {
                Groups.insert {
                    it[Groups.name] = group.name
                    it[Groups.description] = group.description
                    it[Groups.password] = group.password
                } get Groups.id ?: -1
            }
        } catch (e: Throwable) {
            null
        }
    }

    fun addUserToGroupUnchecked(userId: Long, groupId: Int) {
        return transaction {
            UserToGroups.insert {
                it[UserToGroups.groupId] = groupId
                it[UserToGroups.userId] = userId
            }
        }
    }

    fun findGroupMembers(groupId: Int): List<Long> {
        return transaction {
            UserToGroups.select { UserToGroups.groupId.eq(groupId) }
                .map { it[UserToGroups.userId] }
                .toList()
        }
    }

    fun findAllGroups(): List<Group> {
        return transaction {
            Groups.selectAll()
                .map {
                    Group(
                        it[Groups.name],
                        it[Groups.description],
                        it[Groups.id],
                        it[Groups.password]
                    )
                }
        }
    }
}