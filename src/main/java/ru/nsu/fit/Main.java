package ru.nsu.fit;

import java.net.URL;
import java.util.Locale;
import java.util.ResourceBundle;
import javafx.application.Application;
import javafx.application.Platform;
import javafx.event.EventHandler;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.layout.BorderPane;
import javafx.stage.Modality;
import javafx.stage.Stage;
import javafx.stage.WindowEvent;
import ru.nsu.fit.client.CommunicationService;
import ru.nsu.fit.client.CommunicationServiceImpl;
import ru.nsu.fit.client.CreateNewGroupController;
import ru.nsu.fit.client.LoginController;
import ru.nsu.fit.client.MainController;
import ru.nsu.fit.client.RegisterController;
import ru.nsu.fit.client.Session;
import ru.nsu.fit.client.domain.HomediaService;
import ru.nsu.fit.client.player.MediaPlayerController;
import ru.nsu.fit.client.presentation.ChooseGroupController;
import ru.nsu.fit.client.upnp.server.UpnpHomediaService;


public class Main extends Application {
    private static final String TITLE = "Homedia";
    public static final Locale LOCALE = new Locale("en");

    @Override
    public void start(Stage mainStage) throws Exception {
        // main stage
        mainStage.setTitle(TITLE);

        ClassLoader classLoader = getClass().getClassLoader();
        ResourceBundle dictionary = ResourceBundle.getBundle("dictionary/Dictionary", LOCALE, classLoader);

        URL loginViewPath = classLoader.getResource("fxml/login-view.fxml");
        URL registerViewPath = classLoader.getResource("fxml/register-view.fxml");
        URL mainViewPath = classLoader.getResource("fxml/main-view.fxml");
        URL mediaPlayerViewPath = classLoader.getResource("fxml/media-player-view.fxml");
        URL chooseGroupViewPath = classLoader.getResource("fxml/choose-group.fxml");
        URL createNewGroupViewPath = classLoader.getResource("fxml/create-new-group-view.fxml");

        CommunicationService service = new CommunicationServiceImpl("http://127.0.0.1:8080");
        Session session = new Session(service);

        if (dictionary == null ||
                mainViewPath == null ||
                mediaPlayerViewPath == null ||
                createNewGroupViewPath == null ||
                loginViewPath == null ||
                registerViewPath == null ||
                chooseGroupViewPath == null) {
            throw new RuntimeException("Unable to locate resources");
        }

        EventHandler<WindowEvent> windowCloseHandler = event -> {
            Platform.exit();
            System.exit(0);
        };

        FXMLLoader mainLoader = new FXMLLoader(mainViewPath, dictionary);
        BorderPane mainView = mainLoader.load();
        MainController mainController = mainLoader.getController();

        FXMLLoader mediaPlayerLoader = new FXMLLoader(mediaPlayerViewPath, dictionary);
        BorderPane mediaPlayerView = mediaPlayerLoader.load();
        MediaPlayerController mediaPlayerController = mediaPlayerLoader.getController();

        Stage mediaPlayerStage = new Stage();
        mediaPlayerStage.initModality(Modality.APPLICATION_MODAL);
        mediaPlayerStage.setOnCloseRequest(event -> mediaPlayerController.onStop());
        mediaPlayerStage.setScene(new Scene(mediaPlayerView));

        FXMLLoader chooseGroupLoader = new FXMLLoader(chooseGroupViewPath, dictionary);
        BorderPane chooseGroupView = chooseGroupLoader.load();
        ChooseGroupController chooseGroupController = chooseGroupLoader.getController();

        Stage chooseGroupStage = new Stage();
        chooseGroupStage.initModality(Modality.APPLICATION_MODAL);
        chooseGroupStage.setScene(new Scene(chooseGroupView));

        chooseGroupController.setStage(chooseGroupStage);

        HomediaService upnpHomediaService = new UpnpHomediaService();

        mainController.getDevicesTabController().setDeviceService(upnpHomediaService);
        mainController.getDevicesTabController().setMediaPlayerController(mediaPlayerController);
        mainController.getDevicesTabController().setMediaPlayerStage(mediaPlayerStage);

        mainController.getShareTabController().setFileService(session);
        mainController.getShareTabController().setGroupService(service);
        mainController.getShareTabController().setChooseGroupController(chooseGroupController);
        mainController.getShareTabController().setClientState(session);

        mainStage.setScene(new Scene(mainView));
        mainStage.setOnCloseRequest(windowCloseHandler);

        //login stage
        Stage loginStage = new Stage();
        loginStage.setTitle(dictionary.getString("login"));
        FXMLLoader loginLoader = new FXMLLoader(loginViewPath, dictionary);
        BorderPane loginView = loginLoader.load();
        loginStage.setScene(new Scene(loginView));
        loginStage.setOnCloseRequest(windowCloseHandler);

        //register stage
        FXMLLoader registerLoader = new FXMLLoader(registerViewPath, dictionary);
        Parent root = registerLoader.load();
        Stage registerStage = new Stage();
        registerStage.setTitle(dictionary.getString("register"));
        registerStage.setScene(new Scene(root));
        registerStage.setOnCloseRequest(windowCloseHandler);

        //newGroupCreating stage
        FXMLLoader createNewGroupLoader = new FXMLLoader(createNewGroupViewPath, dictionary);
        BorderPane createNewGroupView = createNewGroupLoader.load();
        CreateNewGroupController createNewGroupController = createNewGroupLoader.getController();
        Stage createNewGroupStage = new Stage();
        createNewGroupStage.initModality(Modality.APPLICATION_MODAL);
        createNewGroupStage.setTitle(dictionary.getString("creatingNewGroup"));
        createNewGroupStage.setScene(new Scene(createNewGroupView));
        mainController.getGroupsTabController().setCreateNewGroupStage(createNewGroupStage);
        mainController.getGroupsTabController().setDictionary(dictionary);
        mainController.getGroupsTabController().setService(service);
        mainController.getGroupsTabController().setClientState(session);
        createNewGroupController.setCreateNewGroupStage(createNewGroupStage);
        createNewGroupController.setGroupService(service);
        createNewGroupController.setDictionary(dictionary);
        createNewGroupController.setClientState(session);


        //pass stages to controllers
        RegisterController registerController = registerLoader.getController();
        registerController.setMainStage(mainStage);
        registerController.setLoginStage(loginStage);
        registerController.setDictionary(dictionary);
        registerController.setUserService(service);
        registerController.setState(session);

        LoginController loginController = loginLoader.getController();
        loginController.setMainStage(mainStage);
        loginController.setRegisterStage(registerStage);
        loginController.setDictionary(dictionary);
        loginController.setState(session);
        loginController.setUserService(service);

        loginStage.show();
    }
}


