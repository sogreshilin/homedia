package ru.nsu.fit.interaction;


public interface UserService {
    Long login(String login, String password);
    Long register(UserDto user);
    UserDto findUser(long id);
    boolean hasUserWithLogin(String login);
}


