package ru.nsu.fit.interaction;

import java.util.Objects;

public class GroupDto {
    private String name;
    private String description;
    private String pass;

    public GroupDto(String name, String description, String pass) {
        this.name = name;
        this.description = description;
        this.pass = pass;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof GroupDto)) return false;
        GroupDto groupDto = (GroupDto) o;
        return Objects.equals(name, groupDto.name) &&
                Objects.equals(description, groupDto.description) &&
                Objects.equals(pass, groupDto.pass);
    }

    @Override
    public int hashCode() {
        return Objects.hash(name, description, pass);
    }

    public String getName() {
        return name;
    }

    public String getDescription() {
        return description;
    }

    public String getPass() {
        return pass;
    }

    @Override
    public String toString() {
        return "GroupDto{" +
                "name='" + name + '\'' +
                ", description='" + description + '\'' +
                ", pass='" + pass + '\'' +
                '}';
    }
}
