package ru.nsu.fit.interaction;

public interface GroupListChangedListener {

    void onJoinGroup();

}
