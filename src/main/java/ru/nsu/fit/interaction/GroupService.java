package ru.nsu.fit.interaction;

import ru.nsu.fit.client.domain.UserGroup;

import java.util.List;

public interface GroupService {
    Integer createGroup(GroupDto group);

    Boolean existsWithName(String name);

    /**
     * @param password is optional, null is possible
     * @return true if success
     */
    boolean joinGroup(long userId, int groupId, String password);

    long[] getGroupMemberIds(int groupId);

    int[] getUserGroups(long userId);

    UserGroup getGroupInfo(int groupId);

    List<UserGroup> getAllGroups();

    void addGroupChangeListener(GroupListChangedListener listener);

}
