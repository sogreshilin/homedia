package ru.nsu.fit.interaction;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown = true)
public class UserDto {
    private String name;
    private String login;
    private String pass;

    public UserDto(String name, String login, String pass) {
        this.name = name;
        this.login = login;
        this.pass = pass;
    }

    public UserDto() {
    }

    public String getName() {
        return name;
    }

    public String getLogin() {
        return login;
    }

    public String getPass() {
        return pass;
    }

    @Override
    public String toString() {
        return "UserDto{" + "name='" + name + '\'' + ", login='" + login + '\'' + ", pass='" + pass + '\'' + '}';
    }
}
