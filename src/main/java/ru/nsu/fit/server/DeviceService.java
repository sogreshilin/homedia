package ru.nsu.fit.server;

import java.util.List;

import ru.nsu.fit.client.domain.HomediaDevice;

public interface DeviceService {

    List<HomediaDevice> locateDevices();

    void addDeviceListListener(RemoteDeviceListListener listener);

}
