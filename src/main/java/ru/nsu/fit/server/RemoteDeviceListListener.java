package ru.nsu.fit.server;

public interface RemoteDeviceListListener {

    void onDeviceAdded();

}
