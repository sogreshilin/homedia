package ru.nsu.fit.client;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.Node;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TextField;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import javafx.scene.input.MouseEvent;
import javafx.stage.Stage;
import ru.nsu.fit.interaction.UserService;

import java.util.ResourceBundle;

public class LoginController {
    public Button okButton;

    public void onKeyPressed(KeyEvent keyEvent) {
        if (keyEvent.getCode() == KeyCode.ENTER) {
            handleOKButton(new ActionEvent(keyEvent.getSource(), keyEvent.getTarget()));
        }
    }

    public void onKeyReleased(KeyEvent keyEvent) {
        if (loginField.getText().isEmpty() || passwordField.getText().isEmpty()) {
            okButton.setDisable(true);
        } else {
            okButton.setDisable(false);
        }
    }

    private enum LoginState {
        SUCCESS, NO_USER_EXISTS, BAD_PASSWORD, EMPTY_LOGIN
    }

    private Stage mainStage;

    private Stage registerStage;

    private ResourceBundle dictionary;

    @FXML
    private PasswordField passwordField;

    @FXML
    private TextField loginField;

    private UserService userService;

    private ClientState state;

    @FXML
    void handleOKButton(ActionEvent event) {
        String login = loginField.getText();
        String password = passwordField.getText();

        LoginState registrationState = LoginState.SUCCESS;

        if (login.isEmpty()) {
            registrationState = LoginState.EMPTY_LOGIN;
        } else if (password.isEmpty()) {
            registrationState = LoginState.BAD_PASSWORD;
        } else if (!userService.hasUserWithLogin(login)) {
            registrationState = LoginState.NO_USER_EXISTS;
        } else {
            Long id = userService.login(login, password);
            System.out.println("login with id " + id);
            if (id != null) {
                state.enterWithId(id);
            } else {
                registrationState = LoginState.BAD_PASSWORD;
            }
        }

        if (registrationState == LoginState.SUCCESS) {
            mainStage.show();
            ((Node) event.getSource()).getScene().getWindow().hide();
        } else {
            Alert alert = new Alert(Alert.AlertType.ERROR, dictionary.getString(registrationState.toString()));
            alert.showAndWait();
        }
    }

    @FXML
    void handleRegisterButton(MouseEvent event) {
        ((Node) event.getSource()).getScene().getWindow().hide();
        registerStage.show();
    }

    public void setMainStage(Stage mainStage) {
        this.mainStage = mainStage;
    }

    public void setRegisterStage(Stage registerStage) {
        this.registerStage = registerStage;
    }

    public void setDictionary(ResourceBundle dictionary) {
        this.dictionary = dictionary;
    }

    public void setUserService(UserService userService) {
        this.userService = userService;
    }

    public void setState(ClientState state) {
        this.state = state;
    }
}
