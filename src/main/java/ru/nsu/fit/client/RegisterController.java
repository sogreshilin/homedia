package ru.nsu.fit.client;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.Node;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TextField;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import javafx.scene.input.MouseEvent;
import javafx.stage.Stage;
import ru.nsu.fit.interaction.UserDto;
import ru.nsu.fit.interaction.UserService;

import java.util.ResourceBundle;

public class RegisterController {

    public Button okButton;

    public void onKeyPressed(KeyEvent keyEvent) {
        if (keyEvent.getCode() == KeyCode.ENTER) {
            handleOKButton(new ActionEvent(keyEvent.getSource(), keyEvent.getTarget()));
        }
    }

    public void onKeyReleased(KeyEvent keyEvent) {
        if (loginField.getText().isEmpty() ||
                passwordField.getText().isEmpty() ||
                nameField.getText().isEmpty()) {
            okButton.setDisable(true);
        } else {
            okButton.setDisable(false);
        }
    }

    private enum RegisterState {
        SUCCESS, USER_EXISTS, EMPTY_PASSWORD, EMPTY_NAME, EMPTY_LOGIN, UNKNOWN
    }
    private Stage mainStage;

    private Stage loginStage;

    private ResourceBundle dictionary;

    @FXML
    private PasswordField passwordField;

    @FXML
    private TextField loginField;
    @FXML
    private TextField nameField;

    private UserService userService;

    private ClientState state;

	@FXML
    void handleOKButton(ActionEvent event) {
        String login = loginField.getText();
        String password = passwordField.getText();
        String name = nameField.getText();

        RegisterState registrationState = RegisterState.SUCCESS;

        if (login.isEmpty()) {
            registrationState = RegisterState.EMPTY_LOGIN;
        }
        else if (password.isEmpty()) {
            registrationState = RegisterState.EMPTY_PASSWORD;
        }
        else if (name.isEmpty()) {
            registrationState = RegisterState.EMPTY_NAME;
        }
        else if (userService.hasUserWithLogin(login)) {
            registrationState = RegisterState.USER_EXISTS;
        } else {
            Long id = userService.register(new UserDto(name, login, password));
            if (id != null) {
                state.enterWithId(id);
            } else {
                registrationState = RegisterState.UNKNOWN;
            }
        }

        if (registrationState == RegisterState.SUCCESS) {
            mainStage.show();
            ((Node) event.getSource()).getScene().getWindow().hide();
        } else {
            Alert alert = new Alert(Alert.AlertType.ERROR, dictionary.getString(registrationState.toString()));
            alert.showAndWait();
        }
    }

    public void handleLoginButton(MouseEvent actionEvent) {
        loginStage.show();
        ((Node) actionEvent.getSource()).getScene().getWindow().hide();
    }

    public void setMainStage(Stage mainStage) {
        this.mainStage = mainStage;
    }

    public void setLoginStage(Stage loginStage) {
        this.loginStage = loginStage;
    }

    public void setDictionary(ResourceBundle dictionary) {
        this.dictionary = dictionary;
    }

	public void setUserService(UserService userService) {
		this.userService = userService;
	}

    public void setState(ClientState state) {
        this.state = state;
    }
}
