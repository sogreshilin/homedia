package ru.nsu.fit.client.domain;

import ru.nsu.fit.server.DeviceService;

import java.nio.file.Path;
import java.util.List;

public interface HomediaService extends DeviceService {
    List<HomediaDevice> locateDevices();

    List<HomediaFile> listFiles(Path path);
}
