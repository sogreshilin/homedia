package ru.nsu.fit.client.domain;

public interface HomediaContentFile extends HomediaFile {
    byte[] loadBytes();
}
