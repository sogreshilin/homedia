package ru.nsu.fit.client.domain;

import java.nio.file.Path;

public interface HomediaFile {
    long getId();

    String getName();

    Path getPath();

}
