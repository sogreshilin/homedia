package ru.nsu.fit.client.domain;

import java.util.List;

public interface HomediaDirectory extends HomediaFile {
    List<HomediaFile> listFiles();
}
