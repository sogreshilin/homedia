package ru.nsu.fit.client;

import java.io.File;
import java.util.*;
import java.util.concurrent.ThreadLocalRandom;
import java.util.stream.Collectors;

import ru.nsu.fit.client.domain.UserGroup;
import ru.nsu.fit.interaction.GroupListChangedListener;
import ru.nsu.fit.interaction.GroupService;

public class Session implements ClientState, FileService, GroupListChangedListener {

	private Long userId = null;

	private final GroupService groupService;

	private final Map<Integer, UserGroup> groups = new HashMap<>();
	private final Map<Long, Set<Integer>> fileGroups = new HashMap<>();
	private final Map<Integer, File> sharedFiles = new HashMap<>();

	public Session(GroupService groupService) {
		this.groupService = groupService;
		groupService.addGroupChangeListener(this);
	}

	private void updateGroupsList() {
		Arrays.stream(groupService.getUserGroups(userId))
				.boxed()
				.map(groupService::getGroupInfo)
				.forEach(group -> groups.put(group.getId(), group));
	}

	@Override
	public void enterWithId(long id) {
		userId = id;
		updateGroupsList();
	}

	@Override
	public Long getUserId() {
		return userId;
	}

	@Override
	public int shareFile(File file) {
		int fileId = ThreadLocalRandom.current().nextInt(0, Integer.MAX_VALUE);
		sharedFiles.put(fileId, file);
		return fileId;
	}

	@Override
	public void shareFile(long fileId, int groupId) {
		Set<Integer> current = fileGroups.getOrDefault(fileId, new HashSet<>());
		current.add(groups.get(groupId).getId());
		fileGroups.put(fileId, current);
	}

	@Override
	public List<UserGroup> getFileGroups(long fileId) {
		Set<Integer> _fileGroups = fileGroups.getOrDefault(fileId, new HashSet<>());
		return groups.values().stream().filter(g -> _fileGroups.contains(g.getId())).collect(Collectors.toList());
	}

	@Override
	public Map<Integer, File> getSharedFiles() {
		return sharedFiles;
	}

	public void onJoinGroup() {
		updateGroupsList();
	}
}
