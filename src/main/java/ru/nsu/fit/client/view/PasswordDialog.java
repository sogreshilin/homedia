package ru.nsu.fit.client.view;

import javafx.application.Platform;
import javafx.scene.control.ButtonBar.ButtonData;
import javafx.scene.control.ButtonType;
import javafx.scene.control.Dialog;
import javafx.scene.control.PasswordField;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Priority;

public class PasswordDialog extends Dialog<String> {
    private PasswordField passwordField;

    private PasswordDialog(Builder builder) {
        setTitle(builder.getTitle());
        setHeaderText(builder.getHeaderText());

        ButtonType passwordButtonType = new ButtonType(builder.getOkButtonText(), ButtonData.OK_DONE);
        getDialogPane().getButtonTypes().addAll(passwordButtonType, ButtonType.CANCEL);

        passwordField = new PasswordField();
        passwordField.setPromptText(builder.getPromptText());

        HBox hBox = new HBox();
        hBox.getChildren().add(passwordField);
        HBox.setHgrow(passwordField, Priority.ALWAYS);
        getDialogPane().setContent(hBox);
        Platform.runLater(() -> passwordField.requestFocus());

        setResultConverter(dialogButton -> {
            if (dialogButton == passwordButtonType) {
                return passwordField.getText();
            }
            return null;
        });
    }

    public static Builder builder() {
        return new Builder();
    }

    public PasswordField getPasswordField() {
        return passwordField;
    }

    public static class Builder {
        private String title;
        private String headerText;
        private String promptText;
        private String okButtonText;

        public Builder setTitle(String title) {
            this.title = title;
            return this;
        }

        public Builder setHeaderText(String headerText) {
            this.headerText = headerText;
            return this;
        }

        public Builder setPromptText(String promptText) {
            this.promptText = promptText;
            return this;
        }

        public Builder setOkButtonText(String okButtonText) {
            this.okButtonText = okButtonText;
            return this;
        }

        String getTitle() {
            return title;
        }

        String getHeaderText() {
            return headerText;
        }

        String getPromptText() {
            return promptText;
        }

        public String getOkButtonText() {
            return okButtonText;
        }

        public PasswordDialog build() {
            return new PasswordDialog(this);
        }
    }
}