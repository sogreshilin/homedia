package ru.nsu.fit.client;

import java.util.ResourceBundle;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.scene.input.KeyEvent;
import javafx.stage.Stage;
import ru.nsu.fit.interaction.GroupDto;
import ru.nsu.fit.interaction.GroupService;

public class CreateNewGroupController {
    @FXML
    public TextField groupNameTextField;

    @FXML
    public TextArea groupDescriptionTextArea;

    @FXML
    public PasswordField groupPasswordTextField;

    @FXML
    public Button createButton;

    private Stage createNewGroupStage;

    private GroupService groupService;

    private ResourceBundle dictionary;

    private ClientState clientState;

    public void setCreateNewGroupStage(Stage createNewGroupStage) {
        this.createNewGroupStage = createNewGroupStage;
    }

    public void setGroupService(GroupService groupService) {
        this.groupService = groupService;
    }

    public void setDictionary(ResourceBundle dictionary) {
        this.dictionary = dictionary;
    }

    public void onCancel(ActionEvent actionEvent) {
        groupNameTextField.clear();
        groupDescriptionTextArea.clear();
        createNewGroupStage.hide();
    }

    public void onCreate(ActionEvent actionEvent) {
        String name = groupNameTextField.getText();
        String password = groupPasswordTextField.getText().isEmpty() ? null : groupPasswordTextField.getText();
        String description = groupDescriptionTextArea.getText();

        if (name.isEmpty()) {
            new Alert(Alert.AlertType.ERROR, dictionary.getString("nameMustBeSpecified")).showAndWait();
            return;
        }

        GroupDto groupDto = new GroupDto(name, description, password);

        if (groupService.existsWithName(name)) {
            new Alert(Alert.AlertType.ERROR, dictionary.getString("groupAlreadyExists")).showAndWait();
            return;
        }

        Integer groupId = groupService.createGroup(groupDto);
        if (groupId == null) {
            new Alert(Alert.AlertType.WARNING, dictionary.getString("newGroupWasNotCreated")).showAndWait();
            return;
        }

        long currentUserId = clientState.getUserId();

        boolean joined = groupService.joinGroup(currentUserId, groupId, password);
        if (!joined) {
            new Alert(Alert.AlertType.WARNING, dictionary.getString("userWasNotJoinedTheGroup")).showAndWait();
            createNewGroupStage.hide();
            return;
        }

        createNewGroupStage.hide();
    }

    public void setClientState(ClientState clientState) {
        this.clientState = clientState;
    }

    public void onKeyReleased(KeyEvent keyEvent) {
        if (groupNameTextField.getText().isEmpty()) {
            createButton.setDisable(true);
        } else {
            createButton.setDisable(false);
        }
    }
}
