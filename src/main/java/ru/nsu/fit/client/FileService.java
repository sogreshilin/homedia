package ru.nsu.fit.client;

import java.io.File;
import java.util.List;
import java.util.Map;

import ru.nsu.fit.client.domain.UserGroup;

public interface FileService {

    int shareFile(File file);

    void shareFile(long fileId, int groupId);

    List<UserGroup> getFileGroups(long fileId);

    Map<Integer, File> getSharedFiles();

}
