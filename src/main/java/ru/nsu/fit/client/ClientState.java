package ru.nsu.fit.client;

public interface ClientState {
	void enterWithId(long id);
	Long getUserId();
}
