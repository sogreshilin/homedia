package ru.nsu.fit.client;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.CheckBox;

import ru.nsu.fit.client.presentation.DevicesTabController;
import ru.nsu.fit.client.presentation.GroupsTabController;
import ru.nsu.fit.client.presentation.ShareTabController;

public class MainController {
    @FXML
    public DevicesTabController devicesTabController;

    @FXML
    public GroupsTabController groupsTabController;

    @FXML
    public ShareTabController shareTabController;

    public void onVisibilityChanged(ActionEvent actionEvent) {
        boolean isSelected = ((CheckBox) actionEvent.getTarget()).isSelected();
        // todo: Логика по передаче параметра при изменении видимости устройства туда, куда нужно.
        System.out.println(isSelected);
    }

    public DevicesTabController getDevicesTabController() {
        return devicesTabController;
    }

    public GroupsTabController getGroupsTabController() {
        return groupsTabController;
    }

    public ShareTabController getShareTabController() {
        return shareTabController;
    }


}
