package ru.nsu.fit.client.presentation;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.*;
import java.util.stream.Collectors;

import java.util.stream.IntStream;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.*;
import javafx.scene.input.MouseButton;
import javafx.scene.media.Media;
import javafx.stage.Stage;
import javafx.util.Callback;
import ru.nsu.fit.client.domain.HomediaContentFile;
import ru.nsu.fit.client.domain.HomediaDevice;
import ru.nsu.fit.client.domain.HomediaDirectory;
import ru.nsu.fit.client.domain.HomediaFile;
import ru.nsu.fit.client.player.MediaPlayerController;
import ru.nsu.fit.server.DeviceService;
import ru.nsu.fit.server.RemoteDeviceListListener;

public class DevicesTabController implements RemoteDeviceListListener {

    /* ******* JavaFX dependent fields ******* */

    @FXML
    public ListView<FileCellView> devicesListView;

    @FXML
    public Label deviceNameLabel;

    @FXML
    public Label deviceIdLabel;

    @FXML
    public Label currentDirectory;

    @FXML
    public Button upButton;

    private Stage mediaPlayerStage;

    private ResourceBundle dictionary;

    /* ******* Controller State ******* */

    private MediaPlayerController mediaPlayerController;
    private DeviceService service;

    private Map<Long, HomediaDevice> rootDeviceList;
    private Map<Long, HomediaFile> currentFileModel;
    private FileTree fileTree = FileTree.empty;

    /* ******* DI methods ******* */

    public void setDeviceService(DeviceService service) {
        this.service = service;

        service.addDeviceListListener(this);

        resetDeviceList();
        setCellFactory();
        setOnClickListener();

        upButton.setOnMouseClicked(event -> {
            replaceFileListModel(fileTree.parent.dir.listFiles());
            populateList(new ArrayList<>(currentFileModel.values()));
            fileTree = unnest(fileTree);
        });
    }

    public void setMediaPlayerStage(Stage mediaPlayerStage) {
        this.mediaPlayerStage = mediaPlayerStage;
    }


    public void setMediaPlayerController(MediaPlayerController controller) {
        mediaPlayerController = controller;
    }

    /* ******* Controller initialization methods ******* */

    private FileTree rootFileTree(List<HomediaDevice> devices) {
        return new FileTree(FileTree.empty, new HomediaDirectory() {

            private ResourceBundle bundle = ResourceBundle.getBundle("dictionary/Dictionary", Locale.ENGLISH, getClass().getClassLoader());
            private List<HomediaFile> files = new ArrayList<>(devices);
            private String name = bundle.getString("fileTree.defaultCurrentDir");

            @Override
            public List<HomediaFile> listFiles() {
                return files;
            }

            @Override
            public long getId() {
                return 0;
            }

            @Override
            public String getName() {
                return name;
            }

            @Override
            public Path getPath() {
                return null;
            }

        });
    }

    private void setCellFactory() {
        devicesListView.setCellFactory(new Callback<ListView<FileCellView>, ListCell<FileCellView>>() {

            @Override
            public ListCell<FileCellView> call(ListView<FileCellView> param) {
                return new ListCell<FileCellView>() {

                    @Override
                    protected void updateItem(FileCellView item, boolean empty) {
                        super.updateItem(item, empty);
                        if (item != null) {
                            setText(item.name);
                        }
                    }

                };
            }
        });
    }

    private void setOnClickListener() {
        devicesListView.setOnMouseClicked(mouseEvent -> {
            FileCellView cellView = devicesListView.getSelectionModel().getSelectedItem();
            if (mouseEvent.getButton().equals(MouseButton.PRIMARY)) {
                if (mouseEvent.getClickCount() == 1) {
                    onItemSingleClick(cellView);
                } else if (mouseEvent.getClickCount() == 2) {
                    onItemDoubleClick(cellView);
                }
            }
        });
    }

    /* ******* State control methods ******* */

    private void onItemSingleClick(FileCellView cellView) {
        if (cellView == null) {
            return;
        }

        HomediaFile file = currentFileModel.get(cellView.id);

        if (file instanceof HomediaDevice) {
            HomediaDevice device = (HomediaDevice) file;
            deviceIdLabel.setText(String.valueOf(device.getId()));
            deviceNameLabel.setText(device.getName());
        }
    }

    private void onItemDoubleClick(FileCellView cellView) {
        if (cellView == null) {
            return;
        }

        HomediaFile file = currentFileModel.get(cellView.id);

        if (file instanceof HomediaDirectory) {
            HomediaDirectory chosenDir = (HomediaDirectory) file;
            replaceFileListModel(chosenDir.listFiles());
            populateList(new ArrayList<>(currentFileModel.values()));
            fileTree = nest(fileTree, chosenDir);
        } else if (file instanceof HomediaContentFile){
            downloadFile((HomediaContentFile) file);

            launchMedia(Paths.get(file.getName()));
        }
    }

    private void populateList(List<HomediaFile> files) {
        devicesListView.setEditable(false);
        List<FileCellView> emptyNames = IntStream.range(0, devicesListView.getItems().size())
                .mapToObj(id -> new FileCellView(id, ""))
                .collect(Collectors.toList());
        devicesListView.getItems().clear();
        devicesListView.getItems().addAll(emptyNames);
        devicesListView.getItems().clear();
        devicesListView.getItems().addAll(files.stream()
                .map(d -> new FileCellView(d.getId(), d.getName()))
                .collect(Collectors.toList()));
    }

    private FileTree unnest(FileTree tree) {
        FileTree parent = tree.parent;

        currentDirectory.setText(parent.dir.getName());

        if (parent.parent == FileTree.empty) {
            upButton.setDisable(true);
        }

        return parent;
    }

    private FileTree nest(FileTree tree, HomediaDirectory chosenDir) {
        FileTree child = new FileTree(tree, chosenDir);

        currentDirectory.setText(chosenDir.getName());

        if (child.parent != FileTree.empty) {
            upButton.setDisable(false);
        }

        return child;
    }

    private <T extends HomediaFile> void replaceFileListModel(List<T> files) {
        currentFileModel = files.stream().collect(Collectors.toMap(HomediaFile::getId, file -> file));
    }

    private void launchMedia(Path path) {
        Media media = new Media(path.toUri().toString());
        mediaPlayerController.setMedia(media);
        mediaPlayerStage.show();
    }

    private void resetDeviceList() {
        List<HomediaDevice> devices = service.locateDevices();

        rootDeviceList = devices.stream().collect(Collectors.toMap(HomediaDevice::getId, d -> d));
        fileTree = rootFileTree(devices);
        currentDirectory.setText(fileTree.dir.getName());
        replaceFileListModel(devices);
        populateList(new ArrayList<>(currentFileModel.values()));
    }

    @Override
    public void onDeviceAdded() {
        resetDeviceList();
    }

    private void downloadFile(HomediaContentFile target) {
        byte[] targetFileBytes = target.loadBytes();

        if (null == targetFileBytes) {
            Alert alert = new Alert(Alert.AlertType.ERROR, dictionary.getString("fileDownloadingFailed"));
            alert.showAndWait();
        } else {
            try (FileOutputStream fos = new FileOutputStream(target.getName())) {
                fos.write(targetFileBytes);
            } catch (IOException e) {
                e.printStackTrace();

                Alert alert = new Alert(Alert.AlertType.ERROR, dictionary.getString("fileWritingFailed"));
                alert.showAndWait();
            }
        }
    }

    /* ******* Inner helper classes ******* */

    private static class FileCellView {
        FileCellView(long id, String name) {
            this.id = id;
            this.name = name;
        }

        final long id;
        final String name;
    }

    private static class FileTree {
        final FileTree parent;
        final HomediaDirectory dir;


        FileTree(FileTree parent, HomediaDirectory dir) {
            this.parent = parent;
            this.dir = dir;
        }

        static final FileTree empty = new FileTree(null, null);

    }

    public void setDictionary(ResourceBundle dictionary) {
        this.dictionary = dictionary;
    }

}
