package ru.nsu.fit.client.presentation;

import java.util.List;
import java.util.OptionalInt;
import java.util.stream.Collectors;

import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.ListCell;
import javafx.scene.control.ListView;
import javafx.scene.input.MouseButton;
import javafx.stage.Stage;
import javafx.util.Callback;
import ru.nsu.fit.client.domain.UserGroup;

public class ChooseGroupController {

    @FXML
    ListView<UserGroupCellView> availableGroupsListView;

    @FXML
    Button confirmButton;

    private Stage stage;

    private OptionalInt chosenGroupId;

    public void setStage(Stage stage) {
        this.stage = stage;

        setCellFactory();
        setOnClickListener();
    }

    public OptionalInt display(List<UserGroup> groups) {
        resetState();
        availableGroupsListView.setEditable(false);
        availableGroupsListView.getItems().clear();
        availableGroupsListView.getItems().addAll(groups.stream()
                .map(g -> new UserGroupCellView(g.getId(), g.getName()))
                .collect(Collectors.toList()));

        stage.showAndWait();

        return chosenGroupId;
    }

    private void setCellFactory() {
        availableGroupsListView.setCellFactory(new Callback<ListView<UserGroupCellView>, ListCell<UserGroupCellView>>() {

            @Override
            public ListCell<UserGroupCellView> call(ListView<UserGroupCellView> param) {
                return new ListCell<UserGroupCellView>() {

                    @Override
                    protected void updateItem(UserGroupCellView item, boolean empty) {
                        super.updateItem(item, empty);
                        if (item != null) {
                            setText(item.name);
                        }
                    }

                };
            }
        });
    }

    private void setOnClickListener() {
        availableGroupsListView.setOnMouseClicked(mouseEvent -> {
            UserGroupCellView cellView = availableGroupsListView.getSelectionModel().getSelectedItem();
            if (mouseEvent.getButton().equals(MouseButton.PRIMARY)) {
                if (mouseEvent.getClickCount() == 1) {
                    onItemSingleClick(cellView);
                } else if (mouseEvent.getClickCount() == 2) {
                    onItemDoubleClick(cellView);
                }
            }
        });
    }

    private void onItemSingleClick(UserGroupCellView cellView) {
        chosenGroupId = OptionalInt.of(cellView.id);
        confirmButton.setDisable(false);
    }

    private void onItemDoubleClick(UserGroupCellView cellView) {
        chosenGroupId = OptionalInt.of(cellView.id);
        onConfirm();
    }

    /* ******* Inner helper classes ******* */

    private static class UserGroupCellView {
        UserGroupCellView(int id, String name) {
            this.id = id;
            this.name = name;
        }

        final int id;
        final String name;
    }

    public void onConfirm() {
        stage.hide();
    }

    public void onCancel() {
        resetState();
        stage.hide();
    }

    private void resetState() {
        this.chosenGroupId = OptionalInt.empty();
        this.confirmButton.setDisable(true);
    }

}
