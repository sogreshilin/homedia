package ru.nsu.fit.client.presentation;

import java.io.File;
import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.ListCell;
import javafx.scene.control.ListView;
import javafx.scene.input.MouseButton;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.GridPane;
import javafx.stage.FileChooser;
import javafx.stage.Stage;
import javafx.util.Callback;

import ru.nsu.fit.client.ClientState;
import ru.nsu.fit.client.domain.UserGroup;
import ru.nsu.fit.server.DeviceService;
import ru.nsu.fit.client.FileService;
import ru.nsu.fit.interaction.GroupService;

public class ShareTabController {

    /* ******* JavaFX dependent fields ******* */

    @FXML
    public GridPane gridPane;

    @FXML
    public ListView<FileCellView> devicesListView;

    @FXML
    public ListView<String> sharedGroupsListView;

    @FXML
    public Label currentDirectoryLabel;

    @FXML
    public Label fileNameLabel;

    @FXML
    public Label sidePaneNoteLabel;

    @FXML
    public Button addSharedGroupButton;

    private ChooseGroupController chooseGroupController;

    private Stage mediaPlayerStage;

    /* ******* Controller State ******* */

    private DeviceService deviceService;
    private FileService fileService;
    private GroupService groupService;

    private Map<Integer, File> currentFileModel;
    private Map<Integer, UserGroup> currentGroupModel;

    private FileEntry selectedFile;
    private ClientState clientState;

    /* ******* DI methods ******* */

    public void setFileService(FileService service) {
        this.fileService = service;

        resetFilesList();
        setCellFactory();
        setOnClickListener();
    }

    public void setGroupService(GroupService service) {
        this.groupService = service;
    }

    public void setChooseGroupController(ChooseGroupController controller) {
        this.chooseGroupController = controller;
    }

    public void setClientState(ClientState clientState) {
        this.clientState = clientState;
    }

    /* ******* Controller initialization methods ******* */

    @FXML
    public void initialize() {
        sidePaneNoteLabel.setWrapText(true);
    }

    private void resetFilesList() {
        currentFileModel = fileService.getSharedFiles();
        populateList(currentFileModel);
    }

    private void setCellFactory() {
        devicesListView.setCellFactory(new Callback<ListView<FileCellView>, ListCell<FileCellView>>() {

            @Override
            public ListCell<FileCellView> call(ListView<FileCellView> param) {
                return new ListCell<FileCellView>() {

                    @Override
                    protected void updateItem(FileCellView item, boolean empty) {
                        super.updateItem(item, empty);
                        if (item != null) {
                            setText(item.name);
                        }
                    }

                };
            }
        });
    }

    private void setOnClickListener() {
        devicesListView.setOnMouseClicked(mouseEvent -> {
            FileCellView cellView = devicesListView.getSelectionModel().getSelectedItem();
            if (mouseEvent.getButton().equals(MouseButton.PRIMARY)) {
                if (mouseEvent.getClickCount() == 1) {
                    onItemSingleClick(cellView);
                }
            }
        });
    }

    /* ******* State control methods ******* */

    private void onItemSingleClick(FileCellView cellView) {
        if (cellView == null) {
            sidePaneNoteLabel.setVisible(true);
            gridPane.setVisible(false);
            sharedGroupsListView.setVisible(false);

            return;
        }

        sidePaneNoteLabel.setVisible(false);
        gridPane.setVisible(true);
        sharedGroupsListView.setVisible(true);

        selectedFile = new FileEntry(cellView.id, currentFileModel.get(cellView.id).getName());
        displayGroupsList();
    }

    private void displayGroupsList() {
        List<UserGroup> fileGroups = fileService.getFileGroups(selectedFile.id);
        fileNameLabel.setText(selectedFile.name);
        currentGroupModel = fileGroups.stream().collect(Collectors.toMap(UserGroup::getId, group -> group));
        populateGroupsList(new ArrayList<>(currentGroupModel.values()));
    }

    private void populateList(Map<Integer, File> files) {
        devicesListView.setEditable(false);
        List<FileCellView> emptyNames = IntStream.range(0, devicesListView.getItems().size())
                .mapToObj(id -> new FileCellView(id, ""))
                .collect(Collectors.toList());
        devicesListView.getItems().clear();
        devicesListView.getItems().addAll(emptyNames);

        devicesListView.getItems().clear();
        devicesListView.getItems().addAll(files.entrySet().stream()
                .map(d -> new FileCellView(d.getKey(), d.getValue().getName()))
                .collect(Collectors.toList()));
    }

    private void populateGroupsList(List<UserGroup> groups) {
        sharedGroupsListView.setEditable(false);
        sharedGroupsListView.getItems().clear();
        sharedGroupsListView.getItems().addAll(
                groups.stream()
                        .map(UserGroup::getName)
                        .collect(Collectors.toList())
        );
    }

    public void onShareFileClicked(MouseEvent event) {
        FileChooser chooser = new FileChooser();
        chooser.setTitle("Choose File");
        File file = chooser.showOpenDialog(gridPane.getScene().getWindow());
        if (file != null) {
            fileService.shareFile(file);
            resetFilesList();
        }
    }

    public void onAddSharedGroup(MouseEvent event) {
        long userId = clientState.getUserId();
        int[] userGroupsIds = groupService.getUserGroups(userId);
        List<UserGroup> groups = Arrays
                .stream(userGroupsIds)
                .boxed()
                .map(id -> groupService.getGroupInfo(id))
                .collect(Collectors.toList());
        OptionalInt chosenGroupId = chooseGroupController.display(groups);
        chosenGroupId.ifPresent(groupId -> {
            fileService.shareFile(selectedFile.id, groupId);
            displayGroupsList();
        });
    }

    /* ******* Inner helper classes ******* */

    private static class FileCellView {
        FileCellView(int id, String name) {
            this.id = id;
            this.name = name;
        }

        final int id;
        final String name;
    }

    private static class FileEntry {
        FileEntry(int id, String name) {
            this.id = id;
            this.name = name;
        }

        final int id;
        final String name;
    }

}
