package ru.nsu.fit.client.presentation;

import java.util.List;
import java.util.Objects;
import java.util.Optional;
import java.util.ResourceBundle;
import java.util.stream.Collectors;
import java.util.stream.LongStream;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.ListView;
import javafx.scene.input.MouseButton;
import javafx.scene.input.MouseEvent;
import javafx.stage.Stage;
import ru.nsu.fit.client.ClientState;
import ru.nsu.fit.client.CommunicationService;
import ru.nsu.fit.client.domain.UserGroup;
import ru.nsu.fit.client.view.PasswordDialog;
import ru.nsu.fit.interaction.UserDto;

public class GroupsTabController {
    @FXML
    public Button joinGroupButton;

    @FXML
    public ListView<GroupCellView> groupsListView = new ListView<>();

    @FXML
    public Label nameLabel;

    @FXML
    public Label descriptionLabel;

    @FXML
    public Label membersLabel;

    private Stage createNewGroupStage;
    private CommunicationService communicationService;
    private ResourceBundle dictionary;
    private ClientState clientState;
    private GroupCellView selectedGroupCell;

    public void setDictionary(ResourceBundle dictionary) {
        this.dictionary = dictionary;
    }

    public void setClientState(ClientState clientState) {
        this.clientState = clientState;
    }

    public void setCreateNewGroupStage(Stage createNewGroupStage) {
        this.createNewGroupStage = createNewGroupStage;
    }

    public void setService(CommunicationService communicationService) {
        this.communicationService = communicationService;
        updateGroups();
    }

    public void onCreateNewGroup(ActionEvent actionEvent) {
        createNewGroupStage.showAndWait();
        updateGroups();
    }

    public void onGroupCellClicked(MouseEvent mouseEvent) {
        if (!mouseEvent.getButton().equals(MouseButton.PRIMARY)) {
            return;
        }
        GroupCellView groupCell = groupsListView.getSelectionModel().getSelectedItem();
        if (groupCell != null) {
            selectedGroupCell = groupCell;
        }
        getSelectedGroupCell().ifPresent(cell -> updateGroupCard(cell.groupId));
    }

    public void onJoinGroup(ActionEvent actionEvent) {
        getSelectedGroupCell().ifPresent(cell -> tryJoinGroup(cell.groupId));
    }

    private void tryJoinGroup(int groupId) {
        long currentUserId = clientState.getUserId();
        PasswordDialog dialog = PasswordDialog.builder()
                .setTitle(dictionary.getString("password"))
                .setHeaderText(dictionary.getString("pleaseEnterThePassword"))
                .setPromptText(dictionary.getString("password"))
                .setOkButtonText(dictionary.getString("proceed"))
                .build();
        dialog.showAndWait();

        boolean joined = communicationService.joinGroup(
                currentUserId,
                groupId,
                dialog.getPasswordField().getText()
        );

        if (!joined) {
            new Alert(Alert.AlertType.WARNING, dictionary.getString("unableToJoinGroup")).show();
            return;
        }

        joinGroupButton.setDisable(true);
        updateGroups();
    }

    private Optional<GroupCellView> getSelectedGroupCell() {
        return Optional.ofNullable(selectedGroupCell);
    }

    private void updateGroupCard(int groupId) {
        UserGroup groupInfo = communicationService.getGroupInfo(groupId);
        List<Long> memberIds = LongStream.of(communicationService.getGroupMemberIds(groupId)).boxed().collect(Collectors.toList());
        updateJoinButtonState(memberIds);
        String memberNames = memberIds.stream()
                .map(communicationService::findUser)
                .map(UserDto::getName)
                .sorted()
                .collect(Collectors.joining(", "));

        String notSpecified = dictionary.getString("notSpecified");
        nameLabel.setText(groupInfo.getName().isEmpty() ? notSpecified : groupInfo.getName());
        descriptionLabel.setText(groupInfo.getDescription().isEmpty() ? notSpecified : groupInfo.getDescription());
        membersLabel.setText(memberNames.isEmpty() ? notSpecified : memberNames);
    }

    private void updateGroups() {
        groupsListView.getItems().clear();
        List<UserGroup> groups = communicationService.getAllGroups();
        for (UserGroup group : groups) {
            GroupCellView cellView = new GroupCellView(group.getId(), group.getName(), group.getDescription());
            groupsListView.getItems().add(cellView);
        }
        getSelectedGroupCell().ifPresent(cell -> updateGroupCard(cell.groupId));
    }

    private void updateJoinButtonState(List<Long> memberIds) {
        boolean currentUserBelongsToGroup = memberIds.stream().
                anyMatch(id -> Objects.equals(id, clientState.getUserId()));
        joinGroupButton.setDisable(currentUserBelongsToGroup);
    }

    private class GroupCellView {
        final int groupId;
        final String groupName;
        final String groupDescription;

        GroupCellView(int groupID, String groupName, String groupDescription) {
            this.groupId = groupID;
            this.groupName = groupName;
            this.groupDescription = groupDescription;
        }

        @Override
        public String toString() {
            return GroupsTabController.this.dictionary.getString("groupCell")
                    .replaceFirst("\\{}", String.valueOf(groupId))
                    .replaceFirst("\\{}", groupName);
        }
    }
}
