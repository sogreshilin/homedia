package ru.nsu.fit.client.upnp.server;

import org.fourthline.cling.model.meta.Service;
import ru.nsu.fit.client.domain.HomediaContentFile;
import ru.nsu.fit.client.upnp.client.HomediaUpnpClient;

import java.nio.file.Path;

class MyFancyHomediaContentFile extends MyFancyHomediaFile implements HomediaContentFile {
    private HomediaUpnpClient service = null;

    private Service remoteFileService = null;

    MyFancyHomediaContentFile(long id, String fileName, Path filePath, HomediaUpnpClient targetService, Service s) {
        super(id, fileName, filePath);

        service = targetService;
        remoteFileService = s;
    }

    @Override
    public byte[] loadBytes() {
        if (!service.setChosenFileName(remoteFileService, fileLocation.toString() + "/" + fileName)) {
            System.err.println("Failed to set target file name on service");
            return new byte[0];
        }

        return service.getChosenFile(remoteFileService);
    }
}
