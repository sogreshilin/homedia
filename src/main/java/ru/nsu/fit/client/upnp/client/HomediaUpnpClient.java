package ru.nsu.fit.client.upnp.client;

import org.fourthline.cling.UpnpService;
import org.fourthline.cling.UpnpServiceImpl;
import org.fourthline.cling.controlpoint.ActionCallback;
import org.fourthline.cling.model.action.ActionArgumentValue;
import org.fourthline.cling.model.action.ActionInvocation;
import org.fourthline.cling.model.message.UpnpResponse;
import org.fourthline.cling.model.message.header.STAllHeader;
import org.fourthline.cling.model.meta.RemoteDevice;
import org.fourthline.cling.model.meta.RemoteService;
import org.fourthline.cling.model.meta.Service;
import org.fourthline.cling.model.types.InvalidValueException;
import org.fourthline.cling.model.types.ServiceId;
import org.fourthline.cling.model.types.UDAServiceId;
import org.fourthline.cling.registry.DefaultRegistryListener;
import org.fourthline.cling.registry.Registry;
import org.fourthline.cling.registry.RegistryListener;
import ru.nsu.fit.client.upnp.server.HomediaStreamingService;
import ru.nsu.fit.server.RemoteDeviceListListener;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.Set;

public class HomediaUpnpClient implements Runnable {

    public static void main(String[] args) {
        // Start a user thread that runs the UPnP stack
        Thread clientThread = new Thread(new HomediaUpnpClient());
        clientThread.setDaemon(false);
        clientThread.start();

        try {
            clientThread.join();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    private Set<RemoteDeviceListListener> listeners = new HashSet<>();

    private ArrayList<Service<RemoteDevice, RemoteService>> homediaStreamingServices = new ArrayList<>();

    private UpnpService clientUpnpService = new UpnpServiceImpl();

    public ArrayList<Service<RemoteDevice, RemoteService>> getHomediaStreamingServices() {
        return homediaStreamingServices;
    }

    public void addRemoteDeviceListListener(RemoteDeviceListListener listener) {
        this.listeners.add(listener);
    }

    public void run() {
        try {
            // Add a listener for device registration events
            clientUpnpService.getRegistry().addListener(
                    createRegistryListener(clientUpnpService)
            );

            // Broadcast a search message for all devices
            clientUpnpService.getControlPoint().search(
                    new STAllHeader()
            );

        } catch (Exception ex) {
            System.err.println("Exception occurred: " + ex);
            System.exit(1);
        }
    }

    private RegistryListener createRegistryListener(final UpnpService clientUpnpService) {
        return new DefaultRegistryListener() {

            ServiceId serviceId = new UDAServiceId("HomediaStreamingService");

            @Override
            public void remoteDeviceAdded(Registry registry, RemoteDevice device) {
                Service<RemoteDevice, RemoteService> homediaStreamingService = device.findService(serviceId);

                if (homediaStreamingService != null) {
                    System.out.println("Service discovered: " + homediaStreamingService);

                    homediaStreamingServices.add(homediaStreamingService);
                    listeners.forEach(RemoteDeviceListListener::onDeviceAdded);

//                    String targetFileName = "SOME_RANDOM_FILE.txt";
//                    if (setChosenFileName(clientUpnpService, homediaStreamingService, targetFileName)) {
//                        System.out.println("Set target file name on service");
//                    } else {
//                        System.err.println("Failed to set target file name on service");
//                        return;
//                    }
//
//                    byte[] fileBytes = getChosenFile(clientUpnpService, homediaStreamingService);
//                    System.out.println("Received target file bytes.");
//
//                    File outputFile = new File(targetFileName);
//
//                    try (FileOutputStream stream = new FileOutputStream(outputFile)) {
//                        stream.write(fileBytes);
//                    } catch (IOException e) {
//                        e.printStackTrace();
//                    }
//
//                    System.out.println("Written received file bytes to file.");
//
//                    String[] filenames = getFilesByDirectory(clientUpnpService, homediaStreamingService, ".");
//                    if (filenames.length == 0) {
//                        System.err.println("Failed to get filenames of files in streaming server folder!");
//                    } else {
//                        System.out.println("Files in streaming server folder:");
//                        for (String filename : filenames) {
//                            System.out.println(filename);
//                        }
//                    }
                }
            }

            @Override
            public void remoteDeviceRemoved(Registry registry, RemoteDevice device) {
                Service<RemoteDevice, RemoteService> homediaStreamingService = device.findService(serviceId);

                if (homediaStreamingService != null) {
                    System.out.println("Service disappeared: " + homediaStreamingService);

                    homediaStreamingServices.remove(homediaStreamingService);
                }
            }

        };
    }

    public boolean setChosenFileName(Service<RemoteDevice, RemoteService> homediaStreamingServer, String fileName) {
        return setChosenFileName(clientUpnpService, homediaStreamingServer, fileName);
    }

    private boolean setChosenFileName(UpnpService upnpService, Service<RemoteDevice, RemoteService> homediaStreamingServer, String fileName) {
        ActionInvocation setFileNameAction = new SetFileNameActionInvocation(homediaStreamingServer, fileName);

        Object myFancyMonitor = new Object();

        SetFileNameCallback callback = new SetFileNameCallback(setFileNameAction, myFancyMonitor);

        // Executes asynchronous in the background
        synchronized (myFancyMonitor) {
            upnpService.getControlPoint().execute(callback);

            try {
                myFancyMonitor.wait();
            } catch (InterruptedException e) {
                e.printStackTrace();
                return false;
            }

            if (!callback.getStatus()) {
                return false;
            }
        }

        return true;
    }

    class SetFileNameCallback extends ActionCallback {
        private final Object callbackWaitingMonitor;

        private boolean setFileNameStatus = false;

        SetFileNameCallback(ActionInvocation invocation, final Object waitingMonitor) {
            super(invocation);

            callbackWaitingMonitor = waitingMonitor;
        }

        @Override
        public void success(ActionInvocation invocation) {
            assert invocation.getOutput().length != 0;

            ActionArgumentValue result = invocation.getOutput("TargetFileStatus");
            if ((Boolean) result.getValue()) {
                System.out.println("Successfully set target file name on homedia streaming server!");
                setFileNameStatus = true;
            } else {
                System.out.println("Failed to set target file name on homedia streaming server!");
            }

            synchronized (callbackWaitingMonitor) {
                callbackWaitingMonitor.notify();
            }
        }

        @Override
        public void failure(ActionInvocation invocation,
                            UpnpResponse operation,
                            String defaultMsg) {
            System.err.println(defaultMsg);

            synchronized (callbackWaitingMonitor) {
                callbackWaitingMonitor.notify();
            }
        }

        boolean getStatus() {
            return setFileNameStatus;
        }
    }

    class SetFileNameActionInvocation extends ActionInvocation {
        SetFileNameActionInvocation(Service<RemoteDevice, RemoteService> service, String fileName) {
            super(service.getAction("SetTargetFileName"));

            try {
                // Throws InvalidValueException if the value is of wrong type
                setInput("TargetFileName", fileName);
            } catch (InvalidValueException ex) {
                System.err.println(ex.getMessage());
                System.exit(1);
            }
        }
    }

    public byte[] getChosenFile(Service<RemoteDevice, RemoteService> homediaStreamingServer) {
        return getChosenFile(clientUpnpService, homediaStreamingServer);
    }

    private byte[] getChosenFile(UpnpService upnpService, Service<RemoteDevice, RemoteService> homediaStreamingService) {
        ActionInvocation<RemoteService> getFileInvocation = new ActionInvocation<>(homediaStreamingService.getAction("GetTargetFile"));

        Object myFancyMonitor = new Object();

        GetChosenFileCallback callback = new GetChosenFileCallback(getFileInvocation, myFancyMonitor);

        // Executes asynchronous in the background
        synchronized (myFancyMonitor) {
            upnpService.getControlPoint().execute(callback);

            try {
                myFancyMonitor.wait();
            } catch (InterruptedException e) {
                e.printStackTrace();
                return new byte[0];
            }

            if (callback.getInvocationStatus()) {
                System.out.println("Invocation of Get target file succeed!");

                return callback.getReceivedBytes();
            } else {
                System.out.println("Invocation of Get target file failed!");

                return new byte[0];
            }
        }
    }

    class GetChosenFileCallback extends ActionCallback {
        private final Object callbackWaitingMonitor;

        private boolean status = false;

        private byte[] receivedBytes = null;

        GetChosenFileCallback(ActionInvocation<RemoteService> invocation, Object waitingMonitor) {
            super(invocation);

            callbackWaitingMonitor = waitingMonitor;
        }

        boolean getInvocationStatus() {
            return status;
        }

        byte[] getReceivedBytes() {
            return receivedBytes;
        }

        @Override
        public void success(ActionInvocation invocation) {
            assert invocation.getOutput().length != 0;


            ActionArgumentValue result = invocation.getOutput("FileBytes");

            synchronized (callbackWaitingMonitor) {
                receivedBytes = (byte[]) result.getValue();
                status = true;

                callbackWaitingMonitor.notify();

                System.out.println("Successfully get target file bytes from homedia streaming server!");
            }
        }

        @Override
        public void failure(ActionInvocation invocation,
                            UpnpResponse operation,
                            String defaultMsg) {
            System.err.println(defaultMsg);

            synchronized (callbackWaitingMonitor) {
                callbackWaitingMonitor.notify();
            }
        }
    }

    public String[] getFilesByDirectory(Service<RemoteDevice, RemoteService> homediaStreamingServer, String directory) {
        return getFilesByDirectory(clientUpnpService, homediaStreamingServer, directory);
    }

    private String[] getFilesByDirectory(UpnpService upnpService, Service<RemoteDevice, RemoteService> homediaStreamingService, String directory) {
        ActionInvocation getFilesByDirectoryInvocation = new GetFilenamesByDirectoryActionInvocation(homediaStreamingService, directory);

        Object myFancyMonitor = new Object();

        GetFilesByDirectoryCallback callback = new GetFilesByDirectoryCallback(getFilesByDirectoryInvocation, myFancyMonitor);

        // Executes asynchronous in the background
        synchronized (myFancyMonitor) {
            upnpService.getControlPoint().execute(callback);

            try {
                myFancyMonitor.wait();
            } catch (InterruptedException e) {
                e.printStackTrace();
                return new String[0];
            }

            if (callback.getInvocationStatus()) {
                System.out.println("Invocation of GetNamesOfFilesIn succeed!");

                return callback.getFilenames();
            } else {
                System.out.println("Invocation of GetNamesOfFilesIn failed!");

                return new String[0];
            }
        }
    }

    class GetFilesByDirectoryCallback extends ActionCallback {
        private final Object callbackWaitingMonitor;

        private boolean status = false;

        private String[] filesInDirectory = null;

        GetFilesByDirectoryCallback(ActionInvocation<RemoteService> invocation, Object waitingMonitor) {
            super(invocation);

            callbackWaitingMonitor = waitingMonitor;
        }

        boolean getInvocationStatus() {
            return status;
        }

        String[] getFilenames() {
            return filesInDirectory;
        }

        @Override
        public void success(ActionInvocation invocation) {
            assert invocation.getOutput().length != 0;

            ActionArgumentValue result = invocation.getOutput("FilenamesWithSeparators");

            synchronized (callbackWaitingMonitor) {
                String filenamesWithSeparators = (String) result.getValue();

                if (filenamesWithSeparators.length() == 0) {
                    System.out.println("Received empty string instead of filenames.");

                    status = false;
                    callbackWaitingMonitor.notify();
                    return;
                }

                filesInDirectory = filenamesWithSeparators.split(HomediaStreamingService.SEPARATOR);

                status = true;
                callbackWaitingMonitor.notify();

                System.out.println("Successfully received filenames in directory from homedia streaming server!");
            }
        }

        @Override
        public void failure(ActionInvocation invocation,
                            UpnpResponse operation,
                            String defaultMsg) {
            System.err.println(defaultMsg);

            synchronized (callbackWaitingMonitor) {
                status = false;
                callbackWaitingMonitor.notify();
            }
        }
    }

    class GetFilenamesByDirectoryActionInvocation extends ActionInvocation {
        GetFilenamesByDirectoryActionInvocation(Service<RemoteDevice, RemoteService> service, String directory) {
            super(service.getAction("GetNamesOfFilesIn"));

            try {
                // Throws InvalidValueException if the value is of wrong type
                setInput("TargetDirectoryName", directory);
            } catch (InvalidValueException ex) {
                System.err.println(ex.getMessage());
                System.exit(1);
            }
        }
    }
}
