package ru.nsu.fit.client.upnp.server;

import org.fourthline.cling.model.meta.RemoteDevice;
import org.fourthline.cling.model.meta.RemoteService;
import org.fourthline.cling.model.meta.Service;
import org.fourthline.cling.model.types.UDN;
import ru.nsu.fit.client.domain.*;
import ru.nsu.fit.client.upnp.client.HomediaUpnpClient;
import ru.nsu.fit.server.RemoteDeviceListListener;

import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

public class UpnpHomediaService implements HomediaService {
    private HomediaUpnpClient service = new HomediaUpnpClient();

    private Thread listeningThread = new Thread(service);

    private HomediaSetup localUpnpStreamingDevice = new HomediaSetup();

    private Thread upnpResponseThread = new Thread(localUpnpStreamingDevice);

    private UDN localUpnpDeviceIdentifier = null;

    public UpnpHomediaService() {
        upnpResponseThread.start();
        
        listeningThread.start();
    }

    @Override
    public List<HomediaDevice> locateDevices() {
        List<HomediaDevice> alreadyLocatedDevices = new ArrayList<>();

        ArrayList<Service<RemoteDevice, RemoteService>> services = service.getHomediaStreamingServices();

        for (Service remoteService : services) {
//            if (remoteService.getDevice().getIdentity().getUdn() == localUpnpDeviceIdentifier) {
//                continue;
//            }

            alreadyLocatedDevices.add(new MyFancyHomediaDevice(UUID.randomUUID().getLeastSignificantBits(), remoteService.getServiceId().toString(), service, remoteService));
        }

        return alreadyLocatedDevices;
    }

    @Override
    public void addDeviceListListener(RemoteDeviceListListener listener) {
        service.addRemoteDeviceListListener(listener);
    }

    /**
     * I don't what the hell is this method doing here, sorry.
     */
    @Override
    public List<HomediaFile> listFiles(Path path) {
//        if (service.getHomediaStreamingServices().isEmpty()) {
//            System.err.println("ERROR: There is no loacted devices");
//            return null;
//        }
//
//        String[] filenames = service.getFilesByDirectory(service.getHomediaStreamingServices().get(0), path.toString());
//        if (filenames.length == 0) {
//            System.err.println("Failed to get filenames of files in streaming server folder!");
//        } else {
//            for (String filename : filenames) {
//                filename.replaceAll("\\\\", "/");
//
//                int nameDelimeterPos = filename.lastIndexOf('/');
//
//                if (-1 == nameDelimeterPos) {
//                    System.err.println("WARNING: Received bad filename, skipping");
//                    continue;
//                } else {
//                    String name = filename.substring(nameDelimeterPos);
//                }
//
//            }
//        }
//
        return null;
    }
}

