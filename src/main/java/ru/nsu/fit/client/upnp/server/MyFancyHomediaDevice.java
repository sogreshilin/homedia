package ru.nsu.fit.client.upnp.server;

import org.fourthline.cling.model.meta.Service;
import ru.nsu.fit.client.domain.HomediaDevice;
import ru.nsu.fit.client.domain.HomediaFile;
import ru.nsu.fit.client.upnp.client.HomediaUpnpClient;

import java.nio.file.Paths;
import java.util.List;

class MyFancyHomediaDevice extends MyFancyHomediaDirectory implements HomediaDevice {
    MyFancyHomediaDevice(long id, String name, HomediaUpnpClient targetService, Service s) {
        super(id, name, Paths.get("."), targetService, s);
    }

    @Override
    public List<HomediaFile> listFiles() {
        return getListOfFilesFrom(".");
    }
}
