package ru.nsu.fit.client.upnp.server;

import org.fourthline.cling.UpnpServiceImpl;
import org.fourthline.cling.UpnpService;
import org.fourthline.cling.binding.LocalServiceBindingException;
import org.fourthline.cling.binding.annotations.*;
import org.fourthline.cling.model.DefaultServiceManager;
import org.fourthline.cling.model.ValidationException;
import org.fourthline.cling.model.meta.*;
import org.fourthline.cling.model.types.DeviceType;
import org.fourthline.cling.model.types.UDADeviceType;
import org.fourthline.cling.model.types.UDN;

import java.net.InetAddress;
import java.net.UnknownHostException;


public class HomediaSetup implements Runnable {
    private LocalDevice localUpnpDevice = null;

    public void run() {
        try {

            final UpnpService upnpService = new UpnpServiceImpl();

            Runtime.getRuntime().addShutdownHook(new Thread(upnpService::shutdown));

            localUpnpDevice = createDevice();

            upnpService.getRegistry().addDevice(
                    localUpnpDevice
            );

        } catch (Exception ex) {
            System.err.println("Exception occurred: " + ex);
            ex.printStackTrace(System.err);
            System.exit(1);
        }
    }

    private LocalDevice createDevice()
            throws ValidationException, LocalServiceBindingException {

        DeviceIdentity identity =
                new DeviceIdentity(
                        UDN.uniqueSystemIdentifier("Homedia Streaming Server")
                );

        String hostname = "Unknown";

        try {
            InetAddress addr = InetAddress.getLocalHost();
            hostname = addr.getHostName();
        } catch (UnknownHostException e) {
            e.printStackTrace();
        }


        DeviceType type =
                new UDADeviceType("HomediaStreamingService", 1);

        DeviceDetails details =
                new DeviceDetails(
                        "Homedia streaming server on " + hostname,
                        new ManufacturerDetails("NSU-FIT"),
                        new ModelDetails(
                                "HomediaStreamingServer2000",
                                "Homedia content streaming server.",
                                "v1"
                        )
                );

        LocalService homediaStreamingService =
                new AnnotationLocalServiceBinder().read(HomediaStreamingService.class);

        homediaStreamingService.setManager(
                new DefaultServiceManager(homediaStreamingService, HomediaStreamingService.class)
        );

        return new LocalDevice(identity, type, details, homediaStreamingService);

    }
}
