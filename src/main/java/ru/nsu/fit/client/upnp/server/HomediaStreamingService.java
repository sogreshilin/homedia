package ru.nsu.fit.client.upnp.server;

import org.fourthline.cling.binding.annotations.*;
import org.fourthline.cling.support.contentdirectory.AbstractContentDirectoryService;
import org.fourthline.cling.support.contentdirectory.ContentDirectoryErrorCode;
import org.fourthline.cling.support.contentdirectory.ContentDirectoryException;
import org.fourthline.cling.support.contentdirectory.DIDLParser;
import org.fourthline.cling.support.model.BrowseFlag;
import org.fourthline.cling.support.model.BrowseResult;
import org.fourthline.cling.support.model.DIDLContent;
import org.fourthline.cling.support.model.SortCriterion;
import org.fourthline.cling.support.model.container.Container;
import org.fourthline.cling.support.model.item.Item;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.util.ArrayList;

@UpnpService(
        serviceId = @UpnpServiceId("HomediaStreamingService"),
        serviceType = @UpnpServiceType(value = "HomediaStreamingService", version = 1)
)
public class HomediaStreamingService {

    public HomediaStreamingService() {}

    @UpnpStateVariable()
    private String A_ARG_TYPE_FilenamesWithSeparators;

    @UpnpStateVariable()
    private String A_ARG_TYPE_TargetDirectoryName;

    @UpnpStateVariable()
    private boolean A_ARG_TYPE_TargetFileStatus;

    @UpnpStateVariable()
    private String targetFileName;

    @UpnpStateVariable
    private byte[] fileBytes;

    @UpnpAction(
            out = @UpnpOutputArgument(
                    name = "TargetFileStatus"
            )
    )
    public boolean setTargetFileName(
            @UpnpInputArgument(
                    name = "TargetFileName",
                    stateVariable = "targetFileName"
            ) String fileName)
    {
        targetFileName = fileName;

        File input = new File(targetFileName);
        if (input.isDirectory()) {
            return false;
        }

        try {
            fileBytes = Files.readAllBytes(input.toPath());
        } catch (IOException e) {
            e.printStackTrace();
            return false;
        }

        System.out.println("File loaded.");
        System.out.println("New target file name is: " + fileName);
        return true;
    }

    @UpnpAction(
            out = @UpnpOutputArgument(
                    name = "TargetFileName"
            )
    )
    public String getTargetFileName() {
        return targetFileName;
    }

    @UpnpAction(
            out = @UpnpOutputArgument(
                    name = "FileBytes"
            )
    )
    public byte[] getTargetFile() {
        return fileBytes;
    }

    @UpnpAction(
            out = @UpnpOutputArgument(
                    name = "FilenamesWithSeparators"
            )
    )
    public String getNamesOfFilesIn(@UpnpInputArgument(name = "TargetDirectoryName") String directory)
    {
        File dir = new File(directory);
        if (!dir.isDirectory()) {
            return new String();
        }

        StringBuilder result = new StringBuilder();

        for (final File fileEntry : dir.listFiles()) {
            if (fileEntry.isDirectory()) {
//                getNamesOfFilesIn(fileEntry); //TODO
                result.append(fileEntry.getAbsolutePath());
                result.append(File.separator);
                result.append(SEPARATOR);
            } else {
                result.append(fileEntry.getAbsolutePath());
                result.append(SEPARATOR);
            }
        }

        return result.toString();
    }

    public final static String SEPARATOR = "::";
//
//    // Type
//    enum ObjectType {
//        ROOT_ID,
//        VIDEO_ID,
//        AUDIO_ID,
//        IMAGE_ID
//    }
//
//    // Test
//    public final static String VIDEO_TXT  = "Videos";
//    public final static String AUDIO_TXT  = "Music";
//    public final static String IMAGE_TXT  = "Images";
//
//    // Type subfolder
//    public final static int ALL_ID    = 0;
//    public final static int FOLDER_ID = 1;
//    public final static int ARTIST_ID = 2;
//    public final static int ALBUM_ID  = 3;
//
//    // Prefix item
//    public final static String VIDEO_PREFIX     = "v-";
//    public final static String AUDIO_PREFIX     = "a-";
//    public final static String IMAGE_PREFIX     = "i-";
//    public final static String DIRECTORY_PREFIX = "d-";
//
//    public void setBaseURL(String baseURL) {
//        this.baseURL = baseURL;
//    }
//
//    public BrowseResult browse(String objectID, BrowseFlag browseFlag,
//                               String filter, long firstResult, long maxResults,
//                               SortCriterion[] orderby) throws ContentDirectoryException
//    {
//        System.out.println("Will browse " + objectID);
//
////        try {
////            DIDLContent didl = new DIDLContent();
////
////            String[] objectIDs = objectID.split(Character.toString(SEPARATOR));
////
////            if (0 == objectIDs.length) {
////                throw new ContentDirectoryException(ContentDirectoryErrorCode.CANNOT_PROCESS, "ObjectID Empty!");
////            }
////
////            ObjectType type = ObjectType.values()[Integer.parseInt(objectIDs[0])];
////            if (type != ObjectType.ROOT_ID &&
////                    type != ObjectType.VIDEO_ID &&
////                    type != ObjectType.AUDIO_ID &&
////                    type != ObjectType.IMAGE_ID) {
////                throw new ContentDirectoryException(ContentDirectoryErrorCode.NO_SUCH_OBJECT, "Invalid type!");
////            }
////
////            ArrayList<ObjectType> subtype = new ArrayList<ObjectType>();
////
////            for (int i = 1; i < objectIDs.length; i++) {
////                subtype.add(ObjectType.values()[Integer.parseInt(objectIDs[i])]);
////            }
////
////            Container container = null;
////
////            System.out.println("Browsing type " + type);
////            SharedPreferences sharedPref = PreferenceManager.getDefaultSharedPreferences(ctx);
////
////            Container rootContainer = new CustomContainer( "" + ROOT_ID, "" + ROOT_ID,
////                    ctx.getString(R.string.app_name), ctx.getString(R.string.app_name), baseURL);
////
////
////            // Video
////            Container videoContainer = null;
////            Container allVideoContainer = null;
////            if(sharedPref.getBoolean(SettingsActivity.CONTENTDIRECTORY_VIDEO, true))
////            {
////                videoContainer = new CustomContainer( "" + VIDEO_ID, "" + ROOT_ID,
////                        VIDEO_TXT, ctx.getString(R.string.app_name), baseURL);
////                rootContainer.addContainer(videoContainer);
////                rootContainer.setChildCount(rootContainer.getChildCount()+1);
////
////                allVideoContainer = new VideoContainer( ""+ ALL_ID, "" + VIDEO_ID,
////                        "All", ctx.getString(R.string.app_name), baseURL, ctx);
////                videoContainer.addContainer(allVideoContainer);
////                videoContainer.setChildCount(videoContainer.getChildCount()+1);
////            }
////
////            // Audio
////            Container audioContainer = null;
////            Container artistAudioContainer = null;
////            Container albumAudioContainer = null;
////            Container allAudioContainer = null;
////            if(sharedPref.getBoolean(SettingsActivity.CONTENTDIRECTORY_AUDIO, true))
////            {
////                audioContainer = new CustomContainer( "" + AUDIO_ID, "" + ROOT_ID,
////                        AUDIO_TXT, ctx.getString(R.string.app_name), baseURL);
////                rootContainer.addContainer(audioContainer);
////                rootContainer.setChildCount(rootContainer.getChildCount()+1);
////
////                artistAudioContainer = new ArtistContainer( "" + ARTIST_ID, "" + AUDIO_ID,
////                        "Artist", ctx.getString(R.string.app_name), baseURL, ctx);
////                audioContainer.addContainer(artistAudioContainer);
////                audioContainer.setChildCount(audioContainer.getChildCount()+1);
////
////                albumAudioContainer = new AlbumContainer( "" + ALBUM_ID, "" + AUDIO_ID,
////                        "Album", ctx.getString(R.string.app_name), baseURL, ctx, null);
////                audioContainer.addContainer(albumAudioContainer);
////                audioContainer.setChildCount(audioContainer.getChildCount()+1);
////
////                allAudioContainer = new AudioContainer("" + ALL_ID, "" + AUDIO_ID,
////                        "All", ctx.getString(R.string.app_name), baseURL, ctx, null, null);
////                audioContainer.addContainer(allAudioContainer);
////                audioContainer.setChildCount(audioContainer.getChildCount()+1);
////            }
////
////            // Image
////            Container imageContainer = null;
////            Container allImageContainer = null;
////            if(sharedPref.getBoolean(SettingsActivity.CONTENTDIRECTORY_IMAGE, true))
////            {
////                imageContainer = new CustomContainer( "" + IMAGE_ID, "" + ROOT_ID, IMAGE_TXT,
////                        ctx.getString(R.string.app_name), baseURL);
////                rootContainer.addContainer(imageContainer);
////                rootContainer.setChildCount(rootContainer.getChildCount()+1);
////
////                allImageContainer = new ImageContainer( "" + ALL_ID, "" + IMAGE_ID, "All",
////                        ctx.getString(R.string.app_name), baseURL, ctx);
////                imageContainer.addContainer(allImageContainer);
////                imageContainer.setChildCount(imageContainer.getChildCount()+1);
////            }
////
////            if(subtype.size()==0) {
////                switch (type) {
////                    case ROOT_ID:
////                        container = rootContainer;
////                        break;
////                    case AUDIO_ID:
////                        container = audioContainer;
////             ☺♂♂           break;
////                    case IMAGE_ID:
////                        container = videoContainer;
////                        break;
////                    case VIDEO_ID:
////                        container = imageContainer;
////                        break;
////                }
////            } else {
////                if (type==VIDEO_ID) {
////                    if (subtype.get(0) == ALL_ID) {
////                        System.out.println("Listing all videos...");
////                        container = allVideoContainer;
////                    }
////                } else if(type==AUDIO_ID) {
////                    if(subtype.size()==1) {
////                        if(subtype.get(0) == ARTIST_ID) {
////                            System.out.println("Listing all artists...");
////                            container = artistAudioContainer;
////                        } else if(subtype.get(0) == ALBUM_ID) {
////                            System.out.println("Listing album of all artists...");
////                            container = albumAudioContainer;
////                        } else if(subtype.get(0) == ALL_ID) {
////                            System.out.println("Listing all songs...");
////                            container = allAudioContainer;
////                        }
////                        // and others...
////                    }
////                    else if(subtype.size()==2 && subtype.get(0) == ARTIST_ID)
////                    {
////                        String artistId = "" + subtype.get(1);
////                        String parentId = "" + AUDIO_ID + SEPARATOR + subtype.get(0);
////                        System.out.println("Listing album of artist " + artistId);
////                        container = new AlbumContainer(artistId, parentId, "",
////                                ctx.getString(R.string.app_name), baseURL, ctx, artistId);
////                    }
////                    else if(subtype.size()==2 && subtype.get(0) == ALBUM_ID)
////                    {
////                        String albumId  = "" + subtype.get(1);
////                        String parentId = "" + AUDIO_ID + SEPARATOR + subtype.get(0);
////                        System.out.println("Listing song of album " + albumId);
////                        container = new AudioContainer(albumId, parentId, "",
////                                ctx.getString(R.string.app_name), baseURL, ctx, null, albumId);
////                    }
////                    else if(subtype.size()==3 && subtype.get(0) == ARTIST_ID)
////                    {
////                        String albumId  = "" + subtype.get(2);
////                        String parentId = "" + AUDIO_ID + SEPARATOR + subtype.get(0) + SEPARATOR + subtype.get(1);
////                        System.out.println("Listing song of album " + albumId + " for artist " + subtype.get(1));
////                        container = new AudioContainer(albumId, parentId, "",
////                                ctx.getString(R.string.app_name), baseURL, ctx, null, albumId);
////                    }
////                }
////                else if (type==IMAGE_ID) {
////                    if (subtype.get(0) == ALL_ID) {
////                        System.out.println("Listing all images...");
////                        container = allImageContainer;
////                    }
////                }
////            }
////
////            if (container != null) {
////                System.out.println("List container...");
////
////                // Get container first
////                for (Container c : container.getContainers()) {
////                    didl.addContainer(c);
////                }
////
////                System.out.println("List item...");
////
////                // Then get item
////                for (Item i : container.getItems()) {
////                    didl.addItem(i);
////                }
////
////                System.out.println("Return result...");
////
////                int count = container.getChildCount();
////                System.out.println("Child count : " + count);
////
////                String answer = "";
////                try {
////                    answer = new DIDLParser().generate(didl);
////                } catch (Exception ex) {
////                    throw new ContentDirectoryException(ContentDirectoryErrorCode.CANNOT_PROCESS, ex.toString());
////                }
////                System.out.println("answer : " + answer);
////
////                return new BrowseResult(answer, count, count);
////            }
////        }
////        catch (Exception ex) {
////            throw new ContentDirectoryException(ContentDirectoryErrorCode.CANNOT_PROCESS, ex.toString());
////        }
////
////        System.err.println("No container for this ID !!!");
////        throw new ContentDirectoryException(ContentDirectoryErrorCode.NO_SUCH_OBJECT);
//        return null;
//    }
}
