package ru.nsu.fit.client.upnp.server;

import org.fourthline.cling.model.meta.Service;
import ru.nsu.fit.client.domain.HomediaDirectory;
import ru.nsu.fit.client.domain.HomediaFile;
import ru.nsu.fit.client.upnp.client.HomediaUpnpClient;

import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

class MyFancyHomediaDirectory extends MyFancyHomediaFile implements HomediaDirectory {
    private HomediaUpnpClient service = null;

    private Service remoteFileService = null;

    MyFancyHomediaDirectory(long id, String dirName, Path dirPath, HomediaUpnpClient targetService, Service s) {
        super(id, dirName, dirPath);

        service = targetService;
        remoteFileService = s;
    }

    @Override
    public List<HomediaFile> listFiles() {
        return getListOfFilesFrom(getPath().toString() + "/" + fileName);
    }

    protected List<HomediaFile> getListOfFilesFrom(String path) {
        if (service.getHomediaStreamingServices().isEmpty()) {
            System.err.println("ERROR: There is no located devices");
            return null;
        }

        List<HomediaFile> directoryFiles = new ArrayList<>();

        String[] filenames = service.getFilesByDirectory(remoteFileService, path);
        if (filenames.length == 0) {
            System.err.println("Failed to get filenames of files in streaming server folder!");
        } else {
            for (String filename : filenames) {
                filename = filename.replaceAll("\\\\", "/");

                int nameDelimeterPos = filename.lastIndexOf('/');

                if (-1 == nameDelimeterPos) {
                    System.err.println("WARNING: Received bad filename, skipping");
                    continue;
                }

                if (nameDelimeterPos == filename.length() - 1) {
                    int dirNameDelimeterPos = filename.substring(0, filename.length() - 1).lastIndexOf('/');

                    String dirName = filename.substring(dirNameDelimeterPos + 1, filename.length() - 1);
                    Path dirPath = Paths.get(filename.substring(0, dirNameDelimeterPos + 1));
                    directoryFiles.add(new MyFancyHomediaDirectory(UUID.randomUUID().getLeastSignificantBits(), dirName, dirPath, service, remoteFileService));
                } else {
                    String fileName = filename.substring(nameDelimeterPos + 1);
                    Path filePath = Paths.get(filename.substring(0, nameDelimeterPos + 1));
                    directoryFiles.add(new MyFancyHomediaContentFile(UUID.randomUUID().getLeastSignificantBits(), fileName, filePath, service, remoteFileService));
                }
            }
        }

        return directoryFiles;
    }

    HomediaUpnpClient getService() {
        return service;
    }
}
