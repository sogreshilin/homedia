package ru.nsu.fit.client.upnp.server;

import ru.nsu.fit.client.domain.HomediaFile;

import java.nio.file.Path;

class MyFancyHomediaFile implements HomediaFile {
    //    UUID fileId = UUID.randomUUID();
    long id = 0;
    String fileName = null;
    Path fileLocation = null;

    MyFancyHomediaFile(long fileId, String name, Path path) {
        fileName = name;
        fileLocation = path;
        id = fileId;
    }

    @Override
    public long getId() {
        return id;
    }

    @Override
    public String getName() {
        return fileName;
    }

    @Override
    public Path getPath() {
        return fileLocation;
    }
}
