package ru.nsu.fit.client.player;

import java.util.EnumMap;
import java.util.Map;
import java.util.ResourceBundle;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.media.Media;
import javafx.scene.media.MediaPlayer;
import javafx.scene.media.MediaView;
import javafx.util.Duration;

public class MediaPlayerController {
    private static final double VOLUME_RATE = 1.25;
    private static final double SPEED_RATE = 1.1;
    private final Map<PlayerState, String> toggleButtonText = new EnumMap<>(PlayerState.class);

    @FXML
    public MediaView mediaView;

    @FXML
    public Button toggleButton;

    private MediaPlayer mediaPlayer;

    private Duration currentTime;

    private PlayerState currentState;

    public MediaPlayerController() {
        ResourceBundle dictionary = ResourceBundle.getBundle("dictionary/Dictionary");
        this.toggleButtonText.put(PlayerState.PAUSED, dictionary.getString("play"));
        this.toggleButtonText.put(PlayerState.PLAYING, dictionary.getString("pause"));
    }

    private void onPlay() {
        mediaPlayer.seek(currentTime);
        mediaPlayer.play();
    }

    private void onPause() {
        currentTime = mediaPlayer.getCurrentTime();
        mediaPlayer.pause();
    }

    public void onStop() {
        currentState = PlayerState.PAUSED;
        mediaPlayer.stop();
        currentTime = mediaPlayer.getStartTime();
    }

    public void onVolumeUp() {
        mediaPlayer.setVolume(mediaPlayer.getVolume() * VOLUME_RATE);
    }

    public void onVolumeDown() {
        mediaPlayer.setVolume(mediaPlayer.getVolume() / VOLUME_RATE);
    }

    public void onSpeedUp() {
        mediaPlayer.setRate(mediaPlayer.getRate() * SPEED_RATE);
    }

    public void onSlowDown() {
        mediaPlayer.setRate(mediaPlayer.getRate() / SPEED_RATE);
    }

    public void setMedia(Media media) {
        mediaPlayer = new MediaPlayer(media);
        mediaView.setMediaPlayer(mediaPlayer);
        mediaPlayer.seek(mediaPlayer.getStartTime());
        currentState = PlayerState.PLAYING;
        onToggle();
    }

    public void onToggle() {
        PlayerState nextState;
        if (currentState == PlayerState.PAUSED) {
            nextState = PlayerState.PLAYING;
            onPlay();
        } else {
            nextState = PlayerState.PAUSED;
            onPause();
        }
        toggleButton.setText(toggleButtonText.get(nextState));
        currentState = nextState;
    }
}
