package ru.nsu.fit.client.player;

public enum PlayerState {
    PLAYING, PAUSED
}
